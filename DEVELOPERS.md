# User API Dev Guide
In modern word we really dont need layered structure )
Minimun try-catchs, throwing custom exceptions instead

## Building and deploying

1. Clone git repository
2. Go to repository directory
3. Run "docker-compose up -d" in console
4. Visit "localhost:8181/swagger"
5. .... and please don't forget to switch to v1.1 swagger )

## Additional Information

Small number of unit tests - pretty simple logic, just CRUD
Custom exceptions instead of complicated result objects
Comments - only for visible controllers
Credentials - use from any key vault for all non-dev envs - MUST HAVE