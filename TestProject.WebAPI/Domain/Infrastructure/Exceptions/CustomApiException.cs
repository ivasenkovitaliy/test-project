﻿using System;
using System.Net;
using TestProject.WebAPI.Domain.Infrastructure.Extensions;

namespace TestProject.WebAPI.Domain.Infrastructure.Exceptions
{
    public class CustomApiException : Exception
    {
        public string FriendlyMessage { get; protected set; }
        public HttpStatusCode StatusCode { get; }

        public CustomApiException(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest) : base(message)
        {
            FriendlyMessage = message;
            StatusCode = statusCode;
        }

        public CustomApiException(CustomErrorCodeEnum errorCode = CustomErrorCodeEnum.UnspecifiedError,
            HttpStatusCode statusCode = HttpStatusCode.BadRequest) : base()
        {
            FriendlyMessage = errorCode.GetDescription();
            StatusCode = statusCode;
        }
    }
}
