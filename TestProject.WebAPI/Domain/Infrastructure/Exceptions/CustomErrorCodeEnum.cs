﻿using System.ComponentModel;

namespace TestProject.WebAPI.Domain.Infrastructure.Exceptions
{
    public enum CustomErrorCodeEnum
    {
        /// <summary>
        /// Default error code for custom exception
        /// </summary>
        [Description("Oops, something went wrong with your request, please try again. If this issue persists, please contact us.")]
        UnspecifiedError = 100,


        /// <summary>
        /// Error code for missed user
        /// </summary>
        [Description("User with specific Id not found")]
        UserNotFound = 101,

        /// <summary>
        /// Error code for missed user
        /// </summary>
        [Description("User with specific Email already exist")]
        UserEmailDuplicating = 102,

        /// <summary>
        /// Error code for credit
        /// </summary>
        [Description("User with specific salary and expenses couldn't be created")]
        UserCreditRequirements = 103,
    }
}
