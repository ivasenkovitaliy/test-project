﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TestProject.WebAPI.Domain.DTO;
using TestProject.WebAPI.Domain.Infrastructure.Exceptions;

namespace TestProject.WebAPI.Domain.Infrastructure.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<ExceptionMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (CustomApiException exception)
            {
                context.Response.StatusCode = (int)exception.StatusCode;
                context.Response.ContentType = "application/json";
                var messageDto = new CustomExceptionDTO(exception.FriendlyMessage);
                var jsonObj = JsonConvert.SerializeObject(messageDto);
                
                await context.Response.WriteAsync(jsonObj);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "api error");
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";
                var jsonObj = JsonConvert.SerializeObject(new
                {
                    // it's good idea place here request id generated for non-dev environments with Nginx for example to trak problems later
                });

                await context.Response.WriteAsync(jsonObj);
            }
        }
    }

    public static class CustomExceptionsMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
