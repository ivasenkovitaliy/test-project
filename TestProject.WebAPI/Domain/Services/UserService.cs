﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.Data.Repositories;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Infrastructure.Exceptions;
using TestProject.WebAPI.Domain.Models.v1._1;

namespace TestProject.WebAPI.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task CreateOne(CreateUserModel model)
        {
            if(model.MonthlySalary - model.MonthlyExpenses < 1000)
                throw new CustomApiException(CustomErrorCodeEnum.UserCreditRequirements);

            var user = _mapper.Map<User>(model);
            try
            {
                await _repository.CreateOne(user);
            }
            catch (Exception e)
            {
                // dirty code - time-trial test ))))
                if(e.InnerException.Message.Contains("duplicate key"))
                    throw new CustomApiException(CustomErrorCodeEnum.UserEmailDuplicating);
            }
        }

        public async Task<UserDTO> GetOneWith(int id)
        {
            var user = await _repository.GetOneWith(id);
            var userDto = _mapper.Map<UserDTO>(user);

            return userDto;
        }

        public async Task<IEnumerable<UserDTO>> GetAll(int skip, int take)
        {
            var users = await _repository.GetAll(skip, take);
            var userDtos = _mapper.Map<IEnumerable<UserDTO>>(users);

            return userDtos;
        }
    }

    public interface IUserService
    {
        Task CreateOne(CreateUserModel model);
        Task<UserDTO> GetOneWith(int id);
        Task<IEnumerable<UserDTO>> GetAll(int skip, int take);
    }
}
