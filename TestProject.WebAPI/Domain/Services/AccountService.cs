﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.Data.Repositories;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Models.v1._1;

namespace TestProject.WebAPI.Domain.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IMapper _mapper;

        public AccountService(IUserRepository userRepository, IAccountRepository accountRepository, IMapper mapper)
        {
            _mapper = mapper;
            _accountRepository = accountRepository;
            _userRepository = userRepository;
        }

        public async Task CreateOneFor(CreateAccountModel model)
        {
            var existingUser = await _userRepository.GetOneWith(model.UserId);
            
            var accountToCreate = _mapper.Map<Account>(model);
            accountToCreate.User = existingUser;

            await _accountRepository.CreateOne(accountToCreate);
        }

        public async Task<IEnumerable<AccountDTO>> GetAll(int skip, int take)
        {
            var accounts = await _accountRepository.GetAll(skip, take);
            var dtos = _mapper.Map<IEnumerable<AccountDTO>>(accounts);

            return dtos;
        }
    }

    public interface IAccountService
    {
        Task CreateOneFor(CreateAccountModel model);
        Task<IEnumerable<AccountDTO>> GetAll(int skip, int take);
    }
}
