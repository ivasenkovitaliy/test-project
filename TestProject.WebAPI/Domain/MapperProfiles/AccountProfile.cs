﻿using AutoMapper;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Models.v1._1;

namespace TestProject.WebAPI.Domain.MapperProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<CreateAccountModel, Account>();
            CreateMap<Account, AccountDTO>()
                .ForMember(x => x.Name, opts => opts.MapFrom(src => src.User.Name))
                .ForMember(x => x.Email, opts => opts.MapFrom(src => src.User.Email))
                .ForMember(x => x.MonthlyExpenses, opts => opts.MapFrom(src => src.User.MonthlyExpenses))
                .ForMember(x => x.MonthlyExpenses, opts => opts.MapFrom(src => src.User.MonthlyExpenses));
        }
    }
}
