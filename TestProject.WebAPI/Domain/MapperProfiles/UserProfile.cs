﻿using AutoMapper;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Models.v1._1;

namespace TestProject.WebAPI.Domain.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserModel, User>();
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, DTO.v1._0.UserDTO>();
        }
    }
}
