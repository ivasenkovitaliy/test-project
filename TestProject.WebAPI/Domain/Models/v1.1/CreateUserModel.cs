﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.WebAPI.Domain.Models.v1._1
{
    public class CreateUserModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Only positive number allowed")]
        public decimal MonthlySalary { get; set; }
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Only positive number allowed")]
        public decimal MonthlyExpenses { get; set; }
    }
}
