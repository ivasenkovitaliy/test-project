﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.WebAPI.Domain.Models.v1._1
{
    public class PagedDataModel
    {
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]

        public int Skip { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int Take { get; set; }
    }
}
