﻿namespace TestProject.WebAPI.Domain.Models.v1._1
{
    public class CreateAccountModel
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
