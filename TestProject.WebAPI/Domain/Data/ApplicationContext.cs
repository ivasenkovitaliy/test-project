﻿using Microsoft.EntityFrameworkCore;
using TestProject.WebAPI.Domain.Data.Entities;

namespace TestProject.WebAPI.Domain.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() { }
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseNpgsql("Host=localhost; Port=5433; Database=test; Username=postgres; Password=2pyTmeYMBKE6vJf7; Trust Server Certificate=true;");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(x=>x.Email)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasMany(x => x.Accounts);

            modelBuilder.Entity<Account>()
                .HasOne(x => x.User);
        }
    }
}
