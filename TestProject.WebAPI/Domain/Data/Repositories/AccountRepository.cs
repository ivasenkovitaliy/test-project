﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestProject.WebAPI.Domain.Data.Entities;

namespace TestProject.WebAPI.Domain.Data.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly ApplicationContext _context;

        public AccountRepository(ApplicationContext context)
        {
            _context = context;
        }
        
        public async Task CreateOne(Account account)
        {
            await _context.AddAsync(account);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Account>> GetAll(int skip, int take)
        {
            var accounts = await _context.Accounts
                .Include(x=>x.User)
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            return accounts;
        }
    }

    public interface IAccountRepository
    {
        Task CreateOne(Account account);
        Task<IEnumerable<Account>> GetAll(int skip, int take);
    }
}
