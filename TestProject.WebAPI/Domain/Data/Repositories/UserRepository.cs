﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.Infrastructure.Exceptions;

namespace TestProject.WebAPI.Domain.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _context;

        public UserRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task CreateOne(User user)
        {
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetOneWith(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);

            if(user == null)
                throw new CustomApiException(CustomErrorCodeEnum.UserNotFound);

            return user;
        }

        public async Task<IEnumerable<User>> GetAll(int skip, int take)
        {
            var users = await _context.Users
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            return users;
        }
    }

    public interface IUserRepository
    {
        Task CreateOne(User user);
        Task<User> GetOneWith(int id);
        Task<IEnumerable<User>> GetAll(int skip, int take);
    }
}
