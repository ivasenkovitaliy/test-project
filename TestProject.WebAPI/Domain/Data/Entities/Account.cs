﻿namespace TestProject.WebAPI.Domain.Data.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public AccountType AccountType { get; set; }

        // Account might be used to have different logins in system
        public string Login { get; set; }
        public string HashedPassword { get; set; }

        public User User { get; set; }
    }
}
