﻿using System.Collections.Generic;

namespace TestProject.WebAPI.Domain.Data.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal MonthlySalary { get; set; }
        public decimal MonthlyExpenses { get; set; }

        // Account might be used to have different logins in system
        public IEnumerable<Account> Accounts{ get; set; }
    }
}
