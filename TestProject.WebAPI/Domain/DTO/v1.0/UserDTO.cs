﻿namespace TestProject.WebAPI.Domain.DTO.v1._0
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
