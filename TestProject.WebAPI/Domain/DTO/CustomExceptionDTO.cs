﻿namespace TestProject.WebAPI.Domain.DTO
{
    public class CustomExceptionDTO
    {
        public CustomExceptionDTO(string message)
        {
            FriendlyMessage = message;
        }

        public string FriendlyMessage { get; }
    }
}
