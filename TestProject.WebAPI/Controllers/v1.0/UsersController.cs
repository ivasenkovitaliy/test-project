﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TestProject.WebAPI.Domain.DTO.v1._0;
using TestProject.WebAPI.Domain.Services;

namespace TestProject.WebAPI.Controllers.v1._0
{
    [ApiVersion("1.0", Deprecated = true)]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// User's info
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(UserDTO))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            var userDto = await _userService.GetOneWith(id);

            // emulating backward compatibility.... better choice - use smth like factory of UserService due to version 
            var oldUserDto = _mapper.Map<UserDTO>(userDto);

            return Ok(oldUserDto);
        }
    }
}
