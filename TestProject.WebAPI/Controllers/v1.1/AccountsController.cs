﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Models.v1._1;
using TestProject.WebAPI.Domain.Services;

namespace TestProject.WebAPI.Controllers.v1._1
{
    [ApiVersion("1.1")]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AccountsController : ControllerBase
    { 
        private readonly IAccountService _accountService;

        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Create account for user id
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateUser(CreateAccountModel model)
        {
            await _accountService.CreateOneFor(model);
            return Ok();
        }

        /// <summary>
        /// List users
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<AccountDTO>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] PagedDataModel model)
        {
            var accountDtos = await _accountService.GetAll(model.Skip, model.Take);
            return Ok(accountDtos);
        }
    }
}
