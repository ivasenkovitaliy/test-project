﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TestProject.WebAPI.Domain.DTO.v1._1;
using TestProject.WebAPI.Domain.Models.v1._1;
using TestProject.WebAPI.Domain.Services;

namespace TestProject.WebAPI.Controllers.v1._1
{
    [ApiVersion("1.1")]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> CreateUser(CreateUserModel model)
        {
            await _userService.CreateOne(model);
            return Ok();
        }

        /// <summary>
        /// List users
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(UserDTO))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery]PagedDataModel model)
        {
            var userDtos = await _userService.GetAll(model.Skip, model.Take);
            return Ok(userDtos);
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(UserDTO))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            var userDto = await _userService.GetOneWith(id);
            return Ok(userDto);
        }
    }
}
