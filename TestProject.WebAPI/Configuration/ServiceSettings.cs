﻿namespace TestProject.WebAPI.Configuration
{
    public class ServiceSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }


    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}
