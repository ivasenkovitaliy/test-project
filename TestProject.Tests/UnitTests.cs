using System;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Moq;
using TestProject.WebAPI.Domain.Data.Entities;
using TestProject.WebAPI.Domain.Data.Repositories;
using TestProject.WebAPI.Domain.Infrastructure.Exceptions;
using TestProject.WebAPI.Domain.Models.v1._1;
using TestProject.WebAPI.Domain.Services;
using Xunit;

namespace TestProject.Tests
{
    public class UnitTests
    {
        [Fact]
        public async Task DuplicatedUserEmail_ErrorShouldBeThrown()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(x => x.Map<User>(It.IsAny<CreateUserModel>())).Returns(new User());

            var exception = new Exception("message", new Exception("duplicate key"));
            

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock
                .Setup(x => x.CreateOne(It.IsAny<User>()))
                .Throws(exception);

            var sut = new UserService(userRepositoryMock.Object, mapperMock.Object);

            // Act and Assert
            sut.Invoking(x => x.CreateOne(It.IsAny<CreateUserModel>()))
                .Should().Throw<CustomApiException>();
        }

        [Fact]
        public async Task AnyOtherException_ErrorShouldNotBeThrown()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(x => x.Map<User>(It.IsAny<CreateUserModel>())).Returns(new User());

            var exception = new Exception("message", new Exception("message....."));


            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock
                .Setup(x => x.CreateOne(It.IsAny<User>()))
                .Throws(exception);

            var sut = new UserService(userRepositoryMock.Object, mapperMock.Object);

            // Act and Assert
            sut.Invoking(x => x.CreateOne(It.IsAny<CreateUserModel>()))
                .Should().NotThrow<CustomApiException>();
        }
    }
}